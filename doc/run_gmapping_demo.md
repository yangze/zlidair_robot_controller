## Build and install gmapping package

```shell
cd ~/ros_ws
mkdir ros2_gmapping
cd ros2_gmapping
git clone https://github.com/Project-MANAS/slam_gmapping.git
colcon build
source install/local_setup.zsh
```

## Launch webots simulator

```shell
ros2 launch lidar_robot_controller zlidar_robot_launch.py
```

## Launch gmapping node

```shell
ros2 launch lidar_robot_controller gmapping_demo_launch.py rviz:=true
```

