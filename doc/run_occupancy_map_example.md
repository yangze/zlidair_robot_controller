## Build

```shell
cd ros2_ws
colcon build
```

## Starts webots and ros2 dirver

```shell
ros2 launch lidar_robot_controller zlidar_robot_launch.py
```

## Launch map builder and rviz

```shell
ros2 launch lidar_robot_controller map_visualization_launch.py rviz:=true
```

