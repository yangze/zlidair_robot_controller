#ifndef OBSTACLE_AVOIDANCE_HPP
#define OBSTACLE_AVOIDANCE_HPP

#include "nav_msgs/msg/odometry.hpp"
#include "geometry_msgs/msg/twist.hpp"
#include "sensor_msgs/msg/laser_scan.hpp"
#include "sensor_msgs/msg/point_cloud2.hpp"
#include "sensor_msgs/point_cloud_conversion.hpp"
#include "sensor_msgs/msg/point_cloud.hpp"

#include "rclcpp/rclcpp.hpp"
#include <functional>

class SimpleMoveController : public rclcpp::Node {
public:
  SimpleMoveController() : Node("drive_calibrator") {
    twist_publisher_ =
        this->create_publisher<geometry_msgs::msg::Twist>("/cmd_vel", 10);
    // lidar_scan_subscriber_ =
    //     this->create_subscription<sensor_msgs::msg::LaserScan>(
    //         "/scan", 1,
    //         std::bind(&SimpleMoveController::LaserScanCallback, this,
    //                   std::placeholders::_1));
    lidar_pointcloud2_subscriber_ =
        this->create_subscription<sensor_msgs::msg::PointCloud2>(
            "/scan/point_cloud", 1,
            std::bind(&SimpleMoveController::ScanPointcloudCallback, this,
                      std::placeholders::_1));
    this->declare_parameter<std::string>("calibrate_type", "");
  }

  int SetVelocity(double linear, double angular);
private:
  void LaserScanCallback(sensor_msgs::msg::LaserScan::SharedPtr msg);
  void ScanPointcloudCallback(sensor_msgs::msg::PointCloud2::SharedPtr msg);
private:
  rclcpp::Subscription<nav_msgs::msg::Odometry>::SharedPtr odom_subscriber_;
  rclcpp::Subscription<sensor_msgs::msg::LaserScan>::SharedPtr lidar_scan_subscriber_;
  rclcpp::Subscription<sensor_msgs::msg::PointCloud2>::SharedPtr
      lidar_pointcloud2_subscriber_;

  rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr twist_publisher_;
  geometry_msgs::msg::Twist msg_twist_;
};

#endif
