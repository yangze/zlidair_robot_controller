#include "obstacle_avoidance.hpp"

int SimpleMoveController::SetVelocity(double linear, double angular) {
  msg_twist_.angular.x = 0.0;
  msg_twist_.angular.y = 0.0;
  msg_twist_.angular.z = angular;

  msg_twist_.linear.x = linear;
  msg_twist_.linear.y = 0.0;
  msg_twist_.linear.z = 0.0;

  if (twist_publisher_) {
    twist_publisher_->publish(msg_twist_);
  } else {
    throw std::invalid_argument(
        "in node DriveCalibrator member twist_publisher_ initialize failed");
  }

  return 0;
}

void SimpleMoveController::LaserScanCallback(
    sensor_msgs::msg::LaserScan::SharedPtr msg) {
  for (auto laser_range : msg->ranges) {
    if (laser_range < msg->range_max && laser_range > msg->range_min) {
    }
  }
}

void SimpleMoveController::ScanPointcloudCallback(
    sensor_msgs::msg::PointCloud2::SharedPtr msg) {
  sensor_msgs::msg::PointCloud out_pointcloud;
  sensor_msgs::convertPointCloud2ToPointCloud(*msg, out_pointcloud);

  for (auto pt : out_pointcloud.points) {
    float range = hypotf(pt.x, pt.y);
    if (isinf(range))
      continue;
    
    float angle_rad = atan2(pt.y, pt.x);

    if (angle_rad < 0) {
      angle_rad += 2 * M_PI;
    }

    float angle_deg = angle_rad * 180.0f / M_PI;
    if (angle_deg < 1.0f || angle_deg > 359.0f) {
      RCLCPP_INFO(this->get_logger(), "angle %.4f pt %.4f %.4f", angle_deg,
                  pt.x, pt.y);
    }
  }
}
