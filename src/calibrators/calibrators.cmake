add_executable(drive_calibrator src/calibrators/drive_calibrator.cpp)
target_include_directories(drive_calibrator PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
  $<INSTALL_INTERFACE:include>)
target_compile_features(drive_calibrator PUBLIC c_std_99 cxx_std_17)  # Require C99 and C++17
ament_target_dependencies(
  drive_calibrator
  rclcpp
  pluginlib
  std_msgs
  nav_msgs
  geometry_msgs
  rosgraph_msgs
  sensor_msgs
)

install(TARGETS drive_calibrator
  DESTINATION lib/${PROJECT_NAME})

