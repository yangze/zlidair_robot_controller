#include "drive_calibrator.hpp"

void DriveCalibrator::odometry_callback(
    nav_msgs::msg::Odometry::SharedPtr msg) {
  geometry_msgs::msg::Quaternion q = msg->pose.pose.orientation;
  double yaw =
      atan2(2.0 * (q.w * q.z + q.x * q.y), 1.0 - 2.0 * (q.y * q.y + q.z * q.z));

  if (!odom_params_initialised) {
    if (yaw < 0) {
      yaw += 2 * M_PI;
    }
    odom_params_initialised = true;
    odom_angular_last = yaw;
    odom_angular_start = yaw;
    odom_angular_last_abs = yaw;
    odom_linear_start = msg->pose.pose.position.x;

    time_start_calibrate_ = this->now().nanoseconds();

    bool res = this->get_parameter("calibrate_type", calibrate_type_string_);

    RCLCPP_INFO(this->get_logger(), "initial yaw linear (%f, %f) mode %s %d",
                odom_angular_start, odom_linear_start,
                calibrate_type_string_.c_str(), res);
    return;
  }

  if (yaw - odom_angular_last > M_PI) {
    odom_angular_last_abs += 2 * M_PI - (yaw - odom_angular_last);
  } else if (odom_angular_last - yaw > M_PI) {
    odom_angular_last_abs += 2 * M_PI - (odom_angular_last - yaw);
  } else {
    odom_angular_last_abs += yaw - odom_angular_last;
  }

  if (calibrate_type_string_ == "angular") {
    // angular calibration
    SetVelocity(0.0, kCalibrateAngularSpeed);
    float n_rotations =
        (odom_angular_last_abs - odom_angular_start) / (2 * M_PI);
    if (n_rotations > kMaxNumOfRotations) {
      FinishCalibration();
    }
    RCLCPP_INFO(this->get_logger(), "number of rotattions %.4f yaw %.4f", n_rotations, 
      odom_angular_last * 180.0 / M_PI);
  } else if (calibrate_type_string_ == "linear") {
    SetVelocity(kCalibrateLinearSpeed, 0.0);
    double passed_distance = fabs(msg->pose.pose.position.x - odom_linear_start);
    if (passed_distance > kDefaultDistance) {
      FinishCalibration();
    }
    RCLCPP_INFO(this->get_logger(), "passed distance %.4f", passed_distance);
  }

  // save readings
  odom_angular_last = yaw;
}

int DriveCalibrator::SetVelocity(double linear, double angular) {
  msg_twist_.angular.x = 0.0;
  msg_twist_.angular.y = 0.0;
  msg_twist_.angular.z = angular;

  msg_twist_.linear.x = linear;
  msg_twist_.linear.y = 0.0;
  msg_twist_.linear.z = 0.0;

  if (twist_publisher_) {
    twist_publisher_->publish(msg_twist_);
  } else {
    throw std::invalid_argument(
        "in node DriveCalibrator member twist_publisher_ initialize failed");
  }

  return 0;
}

void DriveCalibrator::FinishCalibration() { 
  SetVelocity(0.0, 0.0);
  rclcpp::shutdown();
}

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include "geometry_msgs/msg/pose.h"

int main(int argc, char **argv) {
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<DriveCalibrator>());
  rclcpp::shutdown();
  return 0;
}

