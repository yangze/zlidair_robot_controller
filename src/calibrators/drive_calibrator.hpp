#ifndef DRIVE_CALIBRATOR_HPP
#define DRIVE_CALIBRATOR_HPP

#include "nav_msgs/msg/odometry.hpp"
#include "geometry_msgs/msg/twist.hpp"

#include "rclcpp/rclcpp.hpp"
#include <functional>

class DriveCalibrator : public rclcpp::Node {
public:
  DriveCalibrator() : Node("drive_calibrator") {
    odom_subscriber_ = this->create_subscription<nav_msgs::msg::Odometry>(
        "/odom", 1,
        std::bind(&DriveCalibrator::odometry_callback, this,
                  std::placeholders::_1));
    twist_publisher_ =
        this->create_publisher<geometry_msgs::msg::Twist>("/cmd_vel", 10);
    this->declare_parameter<std::string>("calibrate_type", "");
  }

  int SetVelocity(double linear, double angular);
private:
  void odometry_callback(nav_msgs::msg::Odometry::SharedPtr msg);

  void FinishCalibration();
private:
  rclcpp::Subscription<nav_msgs::msg::Odometry>::SharedPtr odom_subscriber_;
  rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr twist_publisher_;
  geometry_msgs::msg::Twist msg_twist_;
  bool odom_params_initialised = false;
  double odom_angular_last = 0.0;
  double odom_angular_start = 0.0;
  double odom_angular_last_abs = 0.0;
  double odom_linear_start = 0.0;
  rcl_time_point_value_t time_start_calibrate_ = 0;
  std::string calibrate_type_string_;

  static constexpr float kCalibrateAngularSpeed = 10.0 * M_PI / 180.0;
  static constexpr float kMaxNumOfRotations = 0.5f;
  static constexpr float kCalibrateLinearSpeed = 0.02f;
  static constexpr float kDefaultDistance = 0.1335f;
};

#endif