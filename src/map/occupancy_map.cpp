#include "occupancy_map.hpp"

void OccupancyMapBuilder::DeinitMap(grid_map::GridMap *p_map) {
  if (!p_map)
    return;
  for (grid_map::GridMapIterator it(*p_map); !it.isPastEnd(); ++it) {
    p_map->at("type", *it) = kInitialGridValue4OcupyMap;
  }
}

void OccupancyMapBuilder::LaserScanCallback(
    sensor_msgs::msg::LaserScan::SharedPtr msg) {
  float laser_angle_range = msg->angle_min;

  auto & map_data = (*p_occupancy_map_)["type"];

  for (auto laser_range : msg->ranges) {
    if (laser_range < msg->range_max && laser_range > msg->range_min) {
      float laser_x = laser_range * cos(pose_latest_(2) + laser_angle_range);
      float laser_y = -laser_range * sin(pose_latest_(2) + laser_angle_range);

      float world_x = laser_x + pose_latest_(0);
      float world_y = laser_y + pose_latest_(0);

      GridIndex map_index = World2GridIndex(world_x, world_y);
      GridIndex posi_index = World2GridIndex(pose_latest_(0), pose_latest_(0));
      std::vector<GridIndex> free_index =
          TraceLine(posi_index(0), posi_index(1), map_index(0), map_index(1));

      // update free grids
      for (auto index : free_index) {
        if (IsGridIndexValid(index)) {
          float new_grid_value = map_data(index(0), index(1));

          if (new_grid_value > kThresholdGridNotZero) {
            new_grid_value += kLogFree;
          } else {
            new_grid_value = 0.0f;
          }
          map_data(index(0), index(1)) = new_grid_value;
        }  // end index check
      }

      // update occupied grids
      if (IsGridIndexValid(map_index)) {
        float new_grid_value = map_data(map_index(0), map_index(1));
        if (new_grid_value < kThresholdGridMax) {
          new_grid_value += kLogOcc;
        } else {
          new_grid_value = kThresholdGridMax;
        }
        map_data(map_index(0), map_index(1)) = new_grid_value;
      }
    }  // end valid measurement check
    laser_angle_range += msg->angle_increment;
  }

  PublishMap();
}

void OccupancyMapBuilder::PublishMap() {
  p_occupancy_map_->setTimestamp(this->now().nanoseconds());
  std::unique_ptr<grid_map_msgs::msg::GridMap> message;
  message = grid_map::GridMapRosConverter::toMessage(*p_occupancy_map_);
  grid_map_publisher_->publish(std::move(message));
}

void OccupancyMapBuilder::OdometryCallback(
    nav_msgs::msg::Odometry::SharedPtr msg) {
  geometry_msgs::msg::Quaternion q = msg->pose.pose.orientation;
  double yaw =
      atan2(2.0 * (q.w * q.z + q.x * q.y), 1.0 - 2.0 * (q.y * q.y + q.z * q.z));

  if (!odom_params_initialised) {
    if (yaw < 0) {
      yaw += 2 * M_PI;
    }
    odom_params_initialised = true;
    odom_angular_last = yaw;
    odom_angular_start = yaw;
    odom_angular_last_abs = yaw;
    RCLCPP_INFO(this->get_logger(), "initial yaw %f", odom_angular_start);
    return;
  }

  if (yaw - odom_angular_last > M_PI) {
    odom_angular_last_abs += 2 * M_PI - (yaw - odom_angular_last);
  } else if (odom_angular_last - yaw > M_PI) {
    odom_angular_last_abs += 2 * M_PI - (odom_angular_last - yaw);
  } else {
    odom_angular_last_abs += yaw - odom_angular_last;
  }

  // save pose
  pose_latest_(0) = msg->pose.pose.position.x;
  pose_latest_(1) = msg->pose.pose.position.y;
  pose_latest_(2) = odom_angular_last_abs;

  // save readings
  odom_angular_last = yaw;

  // publish robot global pose
  static int freq_div_factor = 0;
  if (freq_div_factor++ > 10) {
    freq_div_factor = 0;
    robot_pose_stamped_.header.frame_id = "map";
    robot_pose_stamped_.header.stamp = this->now();
    robot_pose_stamped_.pose.position.x = pose_latest_(0);
    robot_pose_stamped_.pose.position.y = pose_latest_(1);
    robot_pose_stamped_.pose.position.z = 0.0;

    tf2::Quaternion q;
    q.setEuler(0.0, 0.0, pose_latest_(2));

    robot_pose_stamped_.pose.orientation.x = q.x();
    robot_pose_stamped_.pose.orientation.y = q.y();
    robot_pose_stamped_.pose.orientation.z = q.z();
    robot_pose_stamped_.pose.orientation.w = q.w();

    pose_stamped_publisher_->publish(robot_pose_stamped_);
  }
}

OccupancyMapBuilder::GridIndex OccupancyMapBuilder::World2GridIndex(double x,
                                                                    double y) {
  GridIndex index{0, 0};
  index(0) =
      std::ceil((x - kMapOriginX) / kMapResolutionInM) + kMapHeightInGrids / 2;
  index(1) =
      std::ceil((y - kMapOriginY) / kMapResolutionInM) + kMapWidthInGrids / 2;

  return index;
}

bool OccupancyMapBuilder::IsGridIndexValid(
    OccupancyMapBuilder::GridIndex index) {
  if (index(0) >= 0 && index(0) < kMapWidthInGrids && index(1) >= 0 &&
      index(1) < kMapHeightInGrids)
    return true;
  return false;
}

std::vector<OccupancyMapBuilder::GridIndex> OccupancyMapBuilder::TraceLine(
    int x0, int y0, int x1, int y1) {
  std::vector<GridIndex> res;

  bool steep = abs(y1 - y0) > abs(x1 - x0);

  // 坐标关于y=x对称 保持斜率小于45度
  if (steep) {
    std::swap(x0, y0);
    std::swap(x1, y1);
  }

  // 保持x0在左侧
  if (x0 > x1) {
    std::swap(x0, x1);
    std::swap(y0, y1);
  }

  int delta_x = x1 - x0;
  int delta_y = abs(y1 - y0);
  int error = 0;
  int y_step = 1;
  int y = y0;

  if (y0 < y1) {
    y_step = 1;
  } else {
    y_step = -1;
  }

  int point_x = 0;
  int point_y = 0;
  for (int x = x0; x <= x1; x++) {
    if (steep) {  // y轴方向为主步进方向
      point_x = y;
      point_y = x;
    } else {
      point_x = x;
      point_y = y;
    }

    error += delta_y;

    if (2 * error >= delta_x) {
      y += y_step;
      error -= delta_x;
    }

    if (point_x == x1 && point_y == y1)
      continue;

    res.push_back(GridIndex(point_x, point_y));
  }
  return res;
}