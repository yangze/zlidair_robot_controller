#ifndef OCCUPANCY_MAP_HPP_
#define OCCUPANCY_MAP_HPP_

#include "rclcpp/rclcpp.hpp"

#include "sensor_msgs/msg/laser_scan.hpp"
#include "nav_msgs/msg/odometry.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"
#include "geometry_msgs/msg/transform_stamped.hpp"
#include "geometry_msgs/msg/twist.hpp"
#include "geometry_msgs/msg/pose.hpp"

// grid map headers
#include "grid_map_core/GridMap.hpp"
#include "grid_map_ros/grid_map_ros.hpp"
#include "grid_map_core/iterators/GridMapIterator.hpp"
#include "grid_map_core/TypeDefs.hpp"

#include <Eigen/Dense>
#include <tf2/LinearMath/Quaternion.h>
#include "tf2_ros/static_transform_broadcaster.h"
#include <exception>

class OccupancyMapBuilder : public rclcpp::Node {
 public:
  OccupancyMapBuilder() : rclcpp::Node("occupancy_map_builder") {
    // lidar_scan_subscriber_ =
    //     this->create_subscription<sensor_msgs::msg::LaserScan>(
    //         "/scan", 1,
    //         std::bind(&OccupancyMapBuilder::LaserScanCallback, this,
    //                   std::placeholders::_1));

    odom_subscriber_ = this->create_subscription<nav_msgs::msg::Odometry>(
        "/odom", 1,
        std::bind(&OccupancyMapBuilder::OdometryCallback, this,
                  std::placeholders::_1));
    p_occupancy_map_ = new grid_map::GridMap({"type"});
    if (!p_occupancy_map_) {
      throw std::runtime_error("Cannot alloc map !");
    }

    p_occupancy_map_->setFrameId("map");
    p_occupancy_map_->setGeometry(
        grid_map::Length(kWorldWidthInM, kWorldHeightInM), kMapResolutionInM);
    RCLCPP_INFO(
        this->get_logger(), "Created map with size %f x %f m (%i x %i cells).",
        p_occupancy_map_->getLength().x(), p_occupancy_map_->getLength().y(),
        p_occupancy_map_->getSize()(0), p_occupancy_map_->getSize()(1));

    grid_map_publisher_ = this->create_publisher<grid_map_msgs::msg::GridMap>(
        "grid_map", rclcpp::QoS(1).transient_local());

    pose_stamped_publisher_ = this->create_publisher<
      geometry_msgs::msg::PoseStamped>("robot_global_pose", 10);

    tf_publisher_ = std::make_shared<tf2_ros::StaticTransformBroadcaster>(this);
    MakeTransform();

    DeinitMap(p_occupancy_map_);
  }

  ~OccupancyMapBuilder() { ; }

  // publish apis
  void PublishMap();

  void MakeTransform() {
    geometry_msgs::msg::TransformStamped tf;

    tf.header.stamp = this->now();
    tf.header.frame_id = "map";
    tf.child_frame_id = "odom";
    tf.transform.translation.x = 0.0;
    tf.transform.translation.y = 0.0;
    tf.transform.translation.z = 0.0;
    tf_publisher_->sendTransform(tf);
  }
  
  // deinit map
  void DeinitMap(grid_map::GridMap *p_map);

  // generate occupancy map
  void LaserScanCallback(sensor_msgs::msg::LaserScan::SharedPtr msg);
  void OdometryCallback(nav_msgs::msg::Odometry::SharedPtr msg);

  using GridIndex = Eigen::Vector2i;

  GridIndex World2GridIndex(double x, double y);
  bool IsGridIndexValid(GridIndex);
  
  std::vector<GridIndex> TraceLine(int x0, int y0, int x1, int y1);

  static constexpr float kWorldWidthInM = 10.0f;
  static constexpr float kWorldHeightInM = 10.0f;
  static constexpr float kMapResolutionInM = 0.05f;
  static constexpr float kWorldCenterXInM = -kWorldWidthInM / 2.0f;
  static constexpr float kMapOriginX = 0.0;
  static constexpr float kMapOriginY = 0.0f;

  static constexpr int kMapWidthInGrids =
      int(kWorldWidthInM / kMapResolutionInM);
  static constexpr int kMapHeightInGrids =
      int(kWorldHeightInM / kMapResolutionInM);

  // log_free and log_occ parameter
  static constexpr float kLogFreeFactor = -1.0;
  static constexpr float kLogFree = kLogFreeFactor / 100.0;
  static constexpr float kLogOccFactor = 2.0;
  static constexpr float kLogOcc = kLogOccFactor / 100.0;
  static constexpr float kInitialGridValue4OcupyMap = 0.5f;
  static constexpr float kThresholdGridNotZero = 0.001f;
  static constexpr float kThresholdGridMax = 1.0f;

 private:
  rclcpp::Subscription<sensor_msgs::msg::LaserScan>::SharedPtr
      lidar_scan_subscriber_;
  rclcpp::Subscription<nav_msgs::msg::Odometry>::SharedPtr odom_subscriber_;
  rclcpp::Publisher<grid_map_msgs::msg::GridMap>::SharedPtr grid_map_publisher_;
  rclcpp::Publisher<geometry_msgs::msg::PoseStamped>::SharedPtr
      pose_stamped_publisher_;
  std::shared_ptr<tf2_ros::StaticTransformBroadcaster> 
      tf_publisher_;
  // odom
  bool odom_params_initialised = false;
  double odom_angular_last = 0.0;
  double odom_angular_start = 0.0;
  double odom_angular_last_abs = 0.0;

  // gobal pose
  Eigen::Vector3d pose_latest_;
  geometry_msgs::msg::PoseStamped robot_pose_stamped_;

  // map
  grid_map::GridMap *p_occupancy_map_ = nullptr;
};

#endif