#include <iostream>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include "geometry_msgs/msg/pose.h"

#include "calibrators/drive_calibrator.hpp"
#include "move_control/obstacle_avoidance.hpp"

int main(int argc, char **argv) {
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<SimpleMoveController>());
  rclcpp::shutdown();
  return 0;
}
