#ifndef ZLIDAR_NOED_HPP_
#define ZLIDAR_NOED_HPP_

#include "rclcpp/rclcpp.hpp"

class ZLidarRobotNode : public rclcpp::Node {
public:
  ZLidarRobotNode() : Node("zlidar_robot") {
    ZLidarRobotInitialize();
  }

  int ZLidarRobotInitialize();
private:
  
};

#endif