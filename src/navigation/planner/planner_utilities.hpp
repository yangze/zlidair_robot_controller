#ifndef PLANNER_UTILITIES_HPP
#define PLANNER_UTILITIES_HPP

#include "planner_base_type.hpp"
#include <queue>

class KeyCompare {
 public:
  bool operator()(const GridIndex &lhs, const GridIndex &rhs) const {
    return (lhs[0] < rhs[0]) || (lhs[0] == rhs[0] && lhs[1] < rhs[1]);
  }
};

struct GriIndexHash {
  std::size_t operator()(const GridIndex& id) const noexcept {
    // NOTE: better to use something like boost hash_combine
    return std::hash<int>()(id[0] ^ (id[1] << 16));
  }
};

// priority queue wrapper
template<typename T, typename priority_t>
struct PriorityQueue {
  typedef std::pair<priority_t, T> PQElement;
  std::priority_queue<PQElement, std::vector<PQElement>,
                 std::greater<PQElement>> elements;

  inline bool empty() const {
     return elements.empty();
  }

  inline void put(T item, priority_t priority) {
    elements.emplace(priority, item);
  }

  T get() {
    T best_item = elements.top().second;
    elements.pop();
    return best_item;
  }
};

#endif