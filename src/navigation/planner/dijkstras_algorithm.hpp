#ifndef DIJKSTRAS_ALGORITHM_HPP
#define DIJKSTRAS_ALGORITHM_HPP

// grid map headers
#include "grid_map_core/GridMap.hpp"
#include "grid_map_ros/grid_map_ros.hpp"
#include "grid_map_core/iterators/GridMapIterator.hpp"
#include "grid_map_core/TypeDefs.hpp"

#include "planner_utilities.hpp"

#include <queue>
#include <vector>
#include <map>
#include <unordered_map>

#include <rclcpp/rclcpp.hpp>

namespace path_planner {
class dijkstras {
 public:
  dijkstras();
  void do_search(grid_map::GridMap &p_map, GridIndex start, GridIndex target,
                 std::vector<GridIndex> &out_path,
                 rclcpp::Node::SharedPtr node);

  std::vector<GridIndex> neighbors(GridIndex index);

  bool in_bounds(const GridIndex index) {
    return index[0] >= 0 && index[0] < map_limit[0] && index[1] >= 0 &&
           index[1] < map_limit[1];
  }

  bool passable(grid_map::Matrix &map_data, const GridIndex index);
 private:
  static constexpr int kNeighborsNum = 4;
  static constexpr float kObstacleThreshold = 0.7f;

  grid_map::Size map_limit;
};
}

#endif  // BREADTH_FIRST_SEARCH
