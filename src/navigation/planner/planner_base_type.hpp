#ifndef PLANNER_BASE_TYPES_HPP_
#define PLANNER_BASE_TYPES_HPP_

// Eigen
#include <Eigen/Core>

using GridIndex = Eigen::Vector2i;

#endif  // PLANNER_BASE_TYPES_HPP_