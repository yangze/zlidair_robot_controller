/*
 * @brief 
 * https://www.redblobgames.com/pathfinding/a-star/introduction.html
 */

#include "breadth_first_search.hpp"

#include "map_utility/patterns.hpp"
namespace path_planner {
breadth_first_search::breadth_first_search() {}
void breadth_first_search::do_search(grid_map::GridMap &map, GridIndex start,
                                     GridIndex target, std::vector<GridIndex> &out_path,
                                     rclcpp::Node::SharedPtr node) {
  map_limit = map.getSize();
  std::queue<GridIndex> frontier;
  std::vector<GridIndex> reached;
  std::map<GridIndex,GridIndex, KeyCompare> came_from;
  std::unordered_map<GridIndex, GridIndex, GriIndexHash> came_from_hash_map;

  grid_map::Matrix &map_data = map["type"];

  frontier.push(start);
  reached.push_back(start);
  came_from[start] = {-1, -1};
  came_from_hash_map[start] = start;

  while (!frontier.empty()) {
    GridIndex current = frontier.front();
    frontier.pop();

    // early exit
    if (current == target) {
      break;
    }
    for (auto next : neighbors(current)) {
      // flood fill example
      // new not in reached
      // if (std::find(reached.begin(), reached.end(), next) ==
      //     reached.end()) {
      //   frontier.push(next);
      //   reached.push_back(next);
      //   // update map data
      //   map_data(next[0], next[1]) = 0.1;
      // }

      // came from example (map example)
      // if (came_from.find(next) == came_from.end()) {
      //   frontier.push(next);
      //   came_from.insert(std::make_pair(next, current));
      //   RCLCPP_INFO(node->get_logger(), "not in reached %d %d  cur %d %d",
      //               next[0], next[1], current[0], current[1]);
      // }

      if (!passable(map_data, next)) {
        RCLCPP_INFO(node->get_logger(), "obstacle %d %d", next[0],
                    next[1]);
        continue;
      }

      // came from example (hash map example)
      if (came_from_hash_map.find(next) == came_from_hash_map.end()) {
        frontier.push(next);
        came_from_hash_map[next] = current;
      }
    }  // end find neighbor
  }

  // reconstruct path
  GridIndex current = target;
  while (current != start) {
    out_path.push_back(current);
    // current = came_from[current];
    current = came_from_hash_map[current];
  }
}

std::vector<GridIndex> breadth_first_search::neighbors(GridIndex index) {
  std::vector<GridIndex> results;

  for (int i = 0; i < kNeighborsNum; i++) {
    GridIndex next{index[0] + kCircularPattern3X3[i][0],
                   index[1] + kCircularPattern3X3[i][1]};
    if (in_bounds(next)) {
      results.push_back(next);
    }
  }

  // ugly path problem
  if ((index[0] + index[1]) % 2 == 0) {
    std::reverse(results.begin(), results.end());
  }
  return results;
}

bool breadth_first_search::passable(grid_map::Matrix &map_data,
                                    const GridIndex index) {
  return map_data(index[0], index[1]) < kObstacleThreshold;
}
}