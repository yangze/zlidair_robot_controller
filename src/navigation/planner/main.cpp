#include "planner_utilities.hpp"
#include <unordered_map>

struct GridLocation {
  int x;
  int y;
};

namespace std {
/* implement hash function so we can put GridLocation into an unordered_set */
template <> struct hash<GridLocation> {
  std::size_t operator()(const GridLocation& id) const noexcept {
    // NOTE: better to use something like boost hash_combine
    return std::hash<int>()(id.x ^ (id.y << 16));
  }
};
}

int main() {
  GridLocation start{1, 4}, goal{8, 3};
  PriorityQueue<GridLocation, double> frontier;
  std::unordered_map<GridLocation, double> cost_so_far;

  frontier.put(goal, 0);
  return 0;
}