#include <iostream>
#include "rclcpp/rclcpp.hpp"
#include "grid_map_core/GridMap.hpp"
#include "grid_map_ros/grid_map_ros.hpp"
#include "grid_map_core/iterators/GridMapIterator.hpp"

int main(int argc, char **argv) {
  rclcpp::init(argc, argv);
  rclcpp::Node::SharedPtr node =
      std::make_shared<rclcpp::Node>("grid_map_simple");
  auto publisher = node->create_publisher<grid_map_msgs::msg::GridMap>(
    "grid_map", rclcpp::QoS(1).transient_local());

  grid_map::GridMap map({"type"});
  map.setFrameId("map");
  map.setGeometry(grid_map::Length(5.0, 5.0), 0.05);

  RCLCPP_INFO(node->get_logger(),
              "Created map with size %f x %f m (%i x %i cells).",
              map.getLength().x(), map.getLength().y(), map.getSize()(0),
              map.getSize()(1));

  rclcpp::Rate rate(30.0);
  rclcpp::Clock clock;
  while (rclcpp::ok()) {
    rclcpp::Time time = node->now();
    for (grid_map::GridMapIterator it(map); !it.isPastEnd(); ++it) {
      grid_map::Position position;
      map.getPosition(*it, position);
      map.at("type", *it) =
          -0.04 + 0.2 * std::sin(3.0 * time.seconds() + 5.0 * position.y()) *
                      position.x();
    }
    // Publish grid map.
    map.setTimestamp(time.nanoseconds());
    std::unique_ptr<grid_map_msgs::msg::GridMap> message;
    message = grid_map::GridMapRosConverter::toMessage(map);
    publisher->publish(std::move(message));
    
    // RCLCPP_INFO(node->get_logger(), "publish once");;

    rate.sleep();
  }
  
  return 0;
}