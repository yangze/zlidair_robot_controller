add_executable(grid_map_example src/examples/main.cpp)

target_include_directories(grid_map_example PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
  $<INSTALL_INTERFACE:include>)
target_compile_features(grid_map_example PUBLIC c_std_99 cxx_std_17)  # Require C99 and C++17
ament_target_dependencies(
  grid_map_example
  rclcpp
  std_msgs
  nav_msgs
  geometry_msgs
  sensor_msgs
  grid_map_core
  grid_map_ros
)

install(TARGETS grid_map_example
  DESTINATION lib/${PROJECT_NAME})


# occupance map build example

add_executable(occupancy_map_example 
  src/examples/occupancy_map_example.cpp
  src/map/occupancy_map.cpp)

target_include_directories(occupancy_map_example PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
  $<BUILD_INTERFACE:${EIGEN3_INCLUDE_DIR}>
  $<INSTALL_INTERFACE:include>)
target_compile_features(occupancy_map_example PUBLIC c_std_99 cxx_std_17)  # Require C99 and C++17
ament_target_dependencies(
  occupancy_map_example
  rclcpp
  std_msgs
  nav_msgs
  geometry_msgs
  sensor_msgs
  grid_map_core
  grid_map_ros
  Eigen3
  cv_bridge
  tf2
  tf2_ros
)

install(TARGETS occupancy_map_example
  DESTINATION lib/${PROJECT_NAME})


# grid map example
add_executable(grid_map_iterator_demo 
  src/examples/iterators_demo_node.cpp
  src/examples/IteratorsDemo.cpp)

target_include_directories(grid_map_iterator_demo PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
  $<BUILD_INTERFACE:${EIGEN3_INCLUDE_DIR}>
  $<INSTALL_INTERFACE:include>)
target_compile_features(grid_map_iterator_demo PUBLIC c_std_99 cxx_std_17)  # Require C99 and C++17
ament_target_dependencies(
  grid_map_iterator_demo
  rclcpp
  std_msgs
  nav_msgs
  geometry_msgs
  sensor_msgs
  grid_map_core
  grid_map_ros
  Eigen3
  cv_bridge
)

install(TARGETS grid_map_iterator_demo
  DESTINATION lib/${PROJECT_NAME})


# global path planner example
set(SEARCH_PROJECT_NAME "breadth_first_search")
add_executable(${SEARCH_PROJECT_NAME} 
src/examples/breadth_first_search_demo.cpp
src/navigation/planner/breadth_first_search.cpp
src/navigation/planner/dijkstras_algorithm.cpp)

target_include_directories(${SEARCH_PROJECT_NAME} PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
  $<BUILD_INTERFACE:${EIGEN3_INCLUDE_DIR}>
  $<INSTALL_INTERFACE:include>
  ${CMAKE_CURRENT_SOURCE_DIR}/src/utility)

target_compile_features(${SEARCH_PROJECT_NAME} PUBLIC c_std_99 cxx_std_17)  # Require C99 and C++17

ament_target_dependencies(
  ${SEARCH_PROJECT_NAME} 
  rclcpp
  std_msgs
  nav_msgs
  geometry_msgs
  sensor_msgs
  grid_map_core
  grid_map_ros
  Eigen3
  cv_bridge
)

install(TARGETS ${SEARCH_PROJECT_NAME} 
DESTINATION lib/${PROJECT_NAME})