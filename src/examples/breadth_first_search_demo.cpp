#include <rclcpp/rclcpp.hpp>
#include "../navigation/planner/breadth_first_search.hpp"
#include "../navigation/planner/dijkstras_algorithm.hpp"

// grid map headers
#include "grid_map_core/GridMap.hpp"
#include "grid_map_ros/grid_map_ros.hpp"
#include "grid_map_core/iterators/GridMapIterator.hpp"
#include "grid_map_core/TypeDefs.hpp"

using namespace path_planner;
using namespace grid_map;

std::vector<GridIndex> obstacles = {{5, 5}, {5, 5}, {6, 5},
                                    {7, 5}, {8, 5}, {9, 5},
                                    {2, 6}, {2, 7}, {2, 8}};

void visualize(rclcpp::Node::SharedPtr node, grid_map::GridMap *p_map,
               rclcpp::Publisher<grid_map_msgs::msg::GridMap>::SharedPtr
                   grid_map_publisher) {
  p_map->setTimestamp(node->now().nanoseconds());
  std::unique_ptr<grid_map_msgs::msg::GridMap> message;
  message = grid_map::GridMapRosConverter::toMessage(*p_map);
  grid_map_publisher->publish(std::move(message));
}

int main(int argc, char **argv) {
  rclcpp::init(argc, argv);

  // search map init
  grid_map::GridMap *p_map = nullptr;
  p_map = new grid_map::GridMap({"type"});
  if (!p_map) {
    throw std::runtime_error("Cannot alloc map !");
  }

  p_map->setFrameId("map");
  p_map->setGeometry(grid_map::Length(1, 1), 0.05);
 
  grid_map::Matrix &map_data = (*p_map)["type"];

  auto map_limit = p_map->getSize();
  for (int x = 0; x < map_limit[0]; x++) {
    for (int y = 0; y < map_limit[1]; y++) {
      map_data(x, y) = 0.1f;
    }
  }
  // draw obstacle to map
  for (auto pt : obstacles) {
    map_data(pt[0], pt[1]) = 0.8f;
  }

  //
  rclcpp::Node::SharedPtr node =
      std::make_shared<rclcpp::Node>("breadth_search_first");
  rclcpp::Publisher<grid_map_msgs::msg::GridMap>::SharedPtr
      grid_map_publisher_ = node->create_publisher<grid_map_msgs::msg::GridMap>(
          "grid_map", rclcpp::QoS(1).transient_local());

  auto mapshow = [&](void) {
    p_map->setTimestamp(node->now().nanoseconds());
    std::unique_ptr<grid_map_msgs::msg::GridMap> message;
    message = grid_map::GridMapRosConverter::toMessage(*p_map);
    grid_map_publisher_->publish(std::move(message));
    return;
  };

  // 
  breadth_first_search path_planner;

  GridIndex start{10, 10};
  GridIndex target{0, 0};

  std::vector<GridIndex> safe_path;
  path_planner.do_search(*p_map, start, target, safe_path, node);

  // visualization
  int show_frames = 0;
  rclcpp::Rate rate_1hz(1.0);
  map_data(start[0], start[1]) = 0.4f;
  map_data(target[0], target[1]) = 0.4f;

  for (auto &pt : safe_path) {
    map_data(pt[0], pt[1]) = 0.4f;
  }

  do {
    mapshow();
    rate_1hz.sleep();
  } while (++show_frames < 15);

  rclcpp::shutdown();
  return 0;
}