#include "rclcpp/rclcpp.hpp"
#include "../map/occupancy_map.hpp"

int main(int argc, char **argv) {
  rclcpp::init(argc, argv);

  rclcpp::spin(std::make_shared<OccupancyMapBuilder>());
  
  rclcpp::shutdown();
  return 0;
}