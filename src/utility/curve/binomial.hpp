#ifndef BINOMIAL_HPP
#define BINOMIAL_HPP

#include <array>
#include <cstddef>

// https://en.wikipedia.org/wiki/Binomial_coefficient
template<int i, int N>
struct Binomial {
  static constexpr int val = static_cast<double>(Binomial<i - 1, N - 1>::val) *
                             static_cast<double>(N) / static_cast<double>(i);
};

// initial value
template<int N>
struct Binomial<0, N> {
  static constexpr int val = 1;
};

// boundary value
template<int N>
struct Binomial<N, N> {
  static constexpr int val = 1;
};

template <size_t... N>
constexpr std::array<int, sizeof...(N) + 1> get_binomial_coefficients_impl(
    std::index_sequence<N...>) {
  constexpr std::array<int, sizeof ... (N) + 1> a = {
    Binomial<N, sizeof ... (N)>::val ...,
    Binomial<sizeof ... (N), sizeof ... (N)>::val
  };
  return a;
}

template <int N>
constexpr std::array<int, N + 1> get_binomial_coefficients() {
  return get_binomial_coefficients_impl(std::make_index_sequence<N>());
}

// get binomial parameter
// typical 3 degree bezier curve example
// p0*(1-t)^3 + 3*p1*t*(1-t)^2 + 3*p2*t^2*(1-t) + p3*t^3
template <size_t N>
constexpr std::tuple<std::array<float, N + 1>, std::array<float, N + 1>>
  BinomialParamterValues(const float t) {
  std::array<float, N + 1> t_values;
  std::array<float, N + 1> one_minus_t_values;

  t_values[0] = 1.0;
  one_minus_t_values[N] = 1.0;

  for (int i = 1; i <= N; i++) {
    t_values[i] = t_values[i - 1] * t;
    one_minus_t_values[N - i] = one_minus_t_values[N - i + 1] * (1.0 - t);
  }
  return {t_values, one_minus_t_values};
}
#endif