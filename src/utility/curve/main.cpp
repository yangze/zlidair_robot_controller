#include <iostream>
#include <array>

#include "bezier.hpp"

using namespace std;

int main2(int argc, char **argv) {
  constexpr size_t degree = 3;
  std::array<Point2f<2>, degree+1> weights = {
    Point2f<2>{-1.0,  0.0},
    Point2f<2>{-0.5,  0.5},
    Point2f<2>{ 0.5, -0.5},
    Point2f<2>{ 1.0,  0.0},
  };
  
  BezierCurve<degree, 10, 2> b(weights);
  b.print();

  auto two_degree_curve = b.get_derivative();
  two_degree_curve.print();

} // namespace std;
