#ifndef BEZIER_HPP
#define BEZIER_HPP

// https://github.com/vss2sn/curves.git  

#include "binomial.hpp"
#include "../utils.hpp"

template <size_t degree, size_t n_points, size_t dimensions>
class BezierCurve {
public:
  constexpr explicit BezierCurve(
      const std::array<Point2f<dimensions>, degree + 1>& weights)
      : weights(weights) {
    constexpr std::array<int, degree + 1> binomial_coefficients =
      get_binomial_coefficients<degree>();

    for (int i = 0; i < n_points; i++) {
      const auto [t_values, one_minus_t_values] = 
        BinomialParamterValues<degree>(i * 1. / (n_points - 1));
      for (int j = 0; j < degree + 1; j++) {
        coefficients[j] =
            binomial_coefficients[j] * t_values[j] * one_minus_t_values[j];
      }
      for (int k = 0; k < dimensions; k++) {  // 2d or 3d
        points[i][k] = 0;

        // apply degree+1 control points to curve
        for (int j = 0; j < degree + 1; j++) {
          points[i][k] += coefficients[j] * weights[j][k];
        }
      }
    }
  }

  constexpr void print() const {
    for (int i = 0; i < n_points; i++) {
      for (int j = 0; j < dimensions; j++) {
        std::cout <<  points[i][j] << "  ";
      }
      std::cout << '\n';
    }
  }

  constexpr BezierCurve<degree - 1, n_points, dimensions> get_derivative()
      const {
    if constexpr (degree > 0) {
      std::array<Point2f<dimensions>, degree> derivative_weights;
      for (int i = 0; i < degree; i++) {
        for (int d = 0; d < dimensions; d++) {
          derivative_weights[i][d] = (weights[i + 1][d] - weights[i][d]);
        }
      }
      return BezierCurve<degree - 1, n_points, dimensions>(derivative_weights);
    } else {
      return BezierCurve<0, n_points, dimensions>();
    }
  }

  constexpr std::array<Point2f<dimensions>, degree + 1> get_weights() const {
    return weights;
  }

private:
  const std::array<Point2f<dimensions>, degree + 1> weights;
  std::array<Point2f<dimensions>, n_points> points;
  std::array<float, degree+1> coefficients;
};

#endif