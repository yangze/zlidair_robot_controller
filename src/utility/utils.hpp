#ifndef UTILS_HPP
#define UTILS_HPP

#include <array>

template <size_t N>
using Point2f = std::array<float, N>;

template <size_t N>
using Point2i = std::array<int, N>;

#endif