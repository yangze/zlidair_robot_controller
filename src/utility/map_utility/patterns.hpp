#ifndef MAP_PATTERNS_HPP_
#define MAP_PATTERNS_HPP_

#include <stdint.h>

constexpr int kCircularPattern3X3[5][2] = {
    {-1, 0}, {0, -1}, {1, 0}, {0, 1}, {0, 0}};

#endif