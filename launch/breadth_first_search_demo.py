from email.policy import default
import os
import pathlib
import launch
from launch_ros.actions import Node
from ament_index_python.packages import get_package_share_directory
from webots_ros2_core.webots_launcher import WebotsLauncher
from launch.substitutions import LaunchConfiguration


def generate_launch_description():
    launch_description_nodes = []
    package_dir = get_package_share_directory('lidar_robot_controller')
    robot_description = pathlib.Path(
        os.path.join(package_dir, 'resource',
                     'lidar_robot.urdf')).read_text()
    ros2_control_params = os.path.join(package_dir, 'resource',
                                       'ros2_control_configuration.yml')

    use_rviz = LaunchConfiguration('rviz', default=False)
    use_sim_time = LaunchConfiguration('use_sim_time', default=True)

    # rviz2 node
    rviz_config = os.path.join(package_dir, 'resource', 'grid_map_iterators_demo.rviz')
    rviz2_node = Node(
        package='rviz2',
        executable='rviz2',
        output='log',
        arguments=['--display-config=' + rviz_config],
        parameters=[{'use_sim_time': use_sim_time}],
        condition=launch.conditions.IfCondition(use_rviz)
    )

    # grid map visualization
    grid_map_visualization_node = Node(
        package='grid_map_visualization',
        executable='grid_map_visualization',
        name='grid_map_visualization',
        output='screen',
        parameters=[ros2_control_params]
    )

    # load zlidar node
    breadth_first_search = Node(
        package='lidar_robot_controller',
        name="breadth_first_search",
        executable='breadth_first_search',
        output='screen',
        parameters=[ros2_control_params],
    )

    # Standard ROS 2 launch description
    return launch.LaunchDescription([
        grid_map_visualization_node,

        breadth_first_search,

        rviz2_node,

        # This action will kill all nodes once the Webots simulation has exited
        launch.actions.
        RegisterEventHandler(event_handler=launch.event_handlers.OnProcessExit(
            on_exit=[launch.actions.EmitEvent(event=launch.events.Shutdown())],
        ))
    ])