from email.policy import default
import os
import pathlib
import launch
from launch_ros.actions import Node
from ament_index_python.packages import get_package_share_directory
from webots_ros2_core.webots_launcher import WebotsLauncher
from launch.substitutions import LaunchConfiguration


def generate_launch_description():
    launch_description_nodes = []
    package_dir = get_package_share_directory('lidar_robot_controller')
    robot_description = pathlib.Path(
        os.path.join(package_dir, 'resource',
                     'lidar_robot.urdf')).read_text()
    ros2_control_params = os.path.join(package_dir, 'resource',
                                       'ros2_control_configuration.yml')

    use_sim_time = LaunchConfiguration('use_sim_time', default=True)
    use_rviz = LaunchConfiguration('rviz', default=False)

    # The WebotsLauncher is a Webots custom action that allows you to start a Webots simulation instance.
    # It searches for the Webots installation in the path specified by the `WEBOTS_HOME` environment variable and default installation paths.
    # The accepted arguments are:
    # - `world` (str): Path to the world to launch.
    # - `gui` (bool): Whether to display GUI or not.
    # - `mode` (str): Can be `pause`, `realtime`, or `fast`.
    webots = WebotsLauncher(
        world=os.path.join(package_dir, 'worlds', 'lidar_robot.wbt'))

    controller_manager_timeout = ['--controller-manager-timeout', '50']
    controller_manager_prefix = 'python.exe' if os.name == 'nt' else ''

    diffdrive_controller_spawner = Node(
        package='controller_manager',
        executable='spawner',
        output='screen',
        prefix=controller_manager_prefix,
        arguments=['diffdrive_controller'] + controller_manager_timeout,
        parameters=[
            {'use_sim_time': use_sim_time},
        ],
    )

    joint_state_broadcaster_spawner = Node(
        package='controller_manager',
        executable='spawner',
        output='screen',
        prefix=controller_manager_prefix,
        arguments=['joint_state_broadcaster'] + controller_manager_timeout,
        parameters=[
            {'use_sim_time': use_sim_time},
        ],
    )

    # The Ros2Supervisor node is a special node interacting with the simulation.
    # For example, it publishes the /clock topic of the simulation or permits to spawn robot from URDF files.
    # By default, its respawn option is set at True.
    # ros2_supervisor = Ros2SupervisorLauncher()  # Only with the develop branch!

    # The node which interacts with a robot in the Webots simulation is located in the `webots_ros2_driver` package under name `driver`.
    # It is necessary to run such a node for each robot in the simulation.
    # Typically, we provide it the `robot_description` parameters from a URDF file, `set_robot_state_publisher` in order to let the node give the URDF generated from Webots to the `robot_state_publisher` and `ros2_control_params` from the `ros2_control` configuration file.
    webots_robot_driver = Node(
        package='webots_ros2_driver',
        executable='driver',
        output='screen',
        parameters=[{
            'robot_description': robot_description,
            'use_sim_time': use_sim_time,
            'set_robot_state_publisher': True},
            ros2_control_params
        ],
        remappings=[
            ('/diffdrive_controller/cmd_vel_unstamped', '/cmd_vel'),
        ]
    )

    # Often we want to publish robot transforms, so we use the `robot_state_publisher` node for that.
    # If robot model is not specified in the URDF file then Webots can help us with the URDF exportation feature.
    # Since the exportation feature is available only once the simulation has started and the `robot_state_publisher` node requires a `robot_description` parameter before we have to specify a dummy robot.
    robot_state_publisher = Node(
        package='robot_state_publisher',
        executable='robot_state_publisher',
        output='screen',
        parameters=[{
            'robot_description':
            '<robot name=""><link name=""/></robot>'
        }],
    )

    # load zlidar node
    zlidar_robot_node = Node(
        package='lidar_robot_controller',
        name="lidar_robot_controller",
        executable='zlidar_robot',
        output='screen',
        parameters=[{
            'robot_description':
            '<robot name=""><link name=""/></robot>'}, 
            ros2_control_params
            ],
    )

    # rviz2 node
    rviz_config = os.path.join(package_dir, 'resource', 'interpolation_demo.rviz')
    rviz2_node = Node(
        package='rviz2',
        executable='rviz2',
        output='log',
        arguments=['--display-config=' + rviz_config],
        parameters=[{'use_sim_time': use_sim_time}],
        condition=launch.conditions.IfCondition(use_rviz)
    )

    # grid map visualization
    grid_map_visualization_node = Node(
        package='grid_map_visualization',
        executable='grid_map_visualization',
        name='grid_map_visualization',
        output='screen',
        parameters=[ros2_control_params]
    )

    # Standard ROS 2 launch description
    return launch.LaunchDescription([
        joint_state_broadcaster_spawner,
        diffdrive_controller_spawner,
        # Start the Webots node
        webots,
        # Start the Ros2Supervisor node
        # ros2_supervisor,  # Only with the develop branch!
        # Start the Webots robot driver
        webots_robot_driver,
        # Start the robot_state_publisher
        robot_state_publisher,
        rviz2_node,
        # This action will kill all nodes once the Webots simulation has exited
        launch.actions.
        RegisterEventHandler(event_handler=launch.event_handlers.OnProcessExit(
            target_action=webots,
            on_exit=[launch.actions.EmitEvent(event=launch.events.Shutdown())],
        ))
    ])