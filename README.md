# lidar_robot_controller



## Overview

This is a controller for **zlidar** robot.It's base on webots 2022a with ros2 package named **[webots_ros2](https://github.com/cyberbotics/webots_ros2)** and ROS2(galactic).

![top_view_world](doc/top_view_world.png)

## Features

- [x] dwa([matlab_demo](doc/run_dwa_demo.md))
- [ ] dwa(webots_demo)
- [ ] VFH+/VFH
- [ ] A*
- [ ] Hybrid A*
- [x] Occupancy Grid Map

## Usage

### Build

```shell
cd ros_ws
# source ros2 enviroments
source ~/ros_galactic/install/setup.zsh
# build the package
colcon build --packages-select lidar_robot_controller
# source the package
source install/local_setup.zsh
```

#### Run

```shell
ros2 launch lidar_robot_controller zlidar_robot_launch.py
```

### VSCode settings

#### set  sys-env ROS2_INSTALL_PATH

If ros2 install from sources, sys-env `ROS2_INSTALL_PATH` in `.bashrc`or`.zshrc`, or you can edit the default setting `.vscode/c_cpp_properties.json`manually.

ex:

```shell
export ROS2_INSTALL_PATH="~/ros2_galactic/install"
```

### colcon arguments complete

```shell
source /usr/local/share/colcon_argcomplete/hook/colcon-argcomplete.zsh
```

## Optional

### Build ros2

```shell
colcon build --symlink-install --merge-install
```

### Build and install webots_ros2_package

```shell
colcon build --symlink-install --merge-install
```

### Build and install grid_map package

[grid_map](https://github.com/ANYbotics/grid_map) package is a C++ library to manage two-dimensional grid maps. More details about please check the repo of grid_map.

### Drive calibration 

1. Launch main node 

   ```shell
   ros2 launch lidar_robot_controller zlidar_robot_launch.py
   ```

2. Run calibrator node

   ```shell
   # calibrate the distance between two wheels
   ros2 run lidar_robot_controller drive_calibrator --ros-args -p calibrate_type:=angular
   # measure wheel radius
   ros2 run lidar_robot_controller drive_calibrator --ros-args -p calibrate_type:=linear
   ```

3. In Webots Node panel ,  from Robot->rotation we can find yaw difference between the real yaw and odom yaw  , also we can find position difference between the real position and odom posirion from Robot->translation

4. Adjust parameters `wheel_separation`  and `wheel_radius` in file `resource/ros2_control_configuration.yml`, repeat steps 1->3 until your are satisfied with the precision.

## Docs

- [Run occupancy map example](doc/run_occupancy_map_example.md)
- [Run Gmapping demo](doc/run_gmapping_demo.md)
- Run breadth search first demo `ros2 launch lidar_robot_controller breadth_first_search_demo.py rviz:=true`

## License

Apache License V2.



