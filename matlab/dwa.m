% https://github.com/AtsushiSakai/MATLABRobotics 
% http://adrianboeing.blogspot.com/2012/05/dynamic-window-algorithm-motion.html
% https://www.cs.princeton.edu/courses/archive/fall11/cos495/COS495-Lecture5-Odometry.pdf

close all

disp('dynamic window approach demo start running')

% param description [x y yaw v w]
x = [0 0 pi/2 0 0]';

point_target = [7, 7];

%% robot kinematic parameters
% [v_max, w_max, va, wa, v_resolution or steps, w_resolution or steps]
kinematic = [2.0, deg2rad(20.0), 0.2, deg2rad(50.0), 0.01, deg2rad(1)];

% eval parameter
% [alpha-heading, belta-dist, gama-velocity, predictDT]
eval_param = [0.19, 0.2, 0.2, 2.0];
% [xmin xmax ymin ymax]
region_xy_m = [-1 11 -1 11];

global dt;
dt = 0.1;

obstacles_point = [
    0 2
    4 2
    5 4
    5 6
    5 8
    8 8
    8 9
    7 9
    6 5
    6 3
    6 8
    7 4
    9 8 
    9 6];
obstacleR = 0.02;

%% main entry
result.x = [];
tic;

for i = 1:5000
    [u, traj] = DynamicWindowApproach(x, kinematic, ...
        point_target, eval_param, obstacles_point, obstacleR);
    % update robot pose use matrix u
    x = f(x, u);

    % save pose info
    result.x = [result.x; x'];

    % if close to target point
    if norm(x(1:2)- point_target') < 0.25
        disp('Arrive target point');
        break;
    end

    % display
    hold off;
    ArrowLength = 0.5;
    quiver(x(1), x(2), ArrowLength * cos(x(3)), ... 
        ArrowLength * sin(x(3)), 'ok');
    hold on;
    plot(result.x(:,1), result.x(:,2), '-b'); hold on;
    plot(point_target(1), point_target(2), '*r'); hold on;
    plot(obstacles_point(:,1), obstacles_point(:,2),'*k'); hold on;

    if ~isempty(traj)
        for it=1:length(traj(:,1)) / 5
            ind = 1 + (it - 1) * 5;
            plot(traj(ind,:),traj(ind+1,:),'-g');hold on;
        end
    end
    axis(region_xy_m);
    grid on;
    drawnow;
end


%%
function [u, trajDB] = DynamicWindowApproach(x, model, goal, ...
    evalParam, ob, R)
% dynamic window [vmin, vmax, wmin, wmax]
Vr = CalcDynamicWindow(x, model);

% evaluation
[evalDB, trajDB] = Evaluation(x, Vr, goal, ob, ...
    R, model, evalParam);

if isempty(evalDB)
    disp('no path to goal!!!');
    u=[0;0];return;
end

% 
evalDB = NormalizeEval(evalDB);

feval = [];
for id = 1:length(evalDB(:, 1))
    feval = [feval; evalParam(1:3) * evalDB(id, 3:5)'];
end

evalDB = [evalDB feval];

[maxv, index] = max(feval);
u = evalDB(index, 1:2)';


end
%% function dynamic window
function Vr = CalcDynamicWindow(x, model)
global dt;

% gen [v_min v_max w_min w_max] array
Vs = [0 model(1) -model(2) model(2)];

% calc dynamic window
Vd = [x(4) - model(3) * dt, x(4) + model(3) * dt, ... 
        x(5) - model(4) * dt, x(5) + model(4) * dt];

Vtemp = [Vs;Vd];
Vr = [max(Vtemp(:, 1)), min(Vtemp(:,2)), ...
        max(Vtemp(:, 3)), min(Vtemp(:,4))];
end

%% function evaluation
function [evalDB, trajDB] = Evaluation(x, Vr, goal, ob, R, model, evalParam)
evalDB = [];
trajDB = [];

for vt = Vr(1):model(5):Vr(2)
    for ot = Vr(3):model(6):Vr(4)
        % trajectory predict and two results will got
        % xt : future pose 
        % traj : trajectory sample
        [xt, traj] = GenerateTrajectory(x, vt, ot, ...
            evalParam(4), model);
        % eval
        heading = CalcHeadingEval(xt, goal);
        dist = CalcDistEval(xt, ob, R);
        vel = abs(vt);
        
        % brake distance
        stopDist = CalcBreakingDist(vel, model);

        if dist > stopDist
            evalDB = [evalDB; [vt ot heading dist vel]];
            trajDB = [trajDB; traj];
        end
    end
end
end

%% function NormalizeEval
function EvalDB = NormalizeEval(EvalDB)
if sum(EvalDB(:,3)) ~= 0
    EvalDB(:,3) = EvalDB(:,3) / sum(EvalDB(:,3));
end
if sum(EvalDB(:,4)) ~= 0
    EvalDB(:,4) = EvalDB(:,4) / sum(EvalDB(:,4));
end
if sum(EvalDB(:,5)) ~= 0
    EvalDB(:,5) = EvalDB(:,5) / sum(EvalDB(:,5));
end
end

%% function generate tragectory
function [x, traj] = GenerateTrajectory(x, vt, wt, evaldt, model)
% vt : current linear speed
% wt : current angular speed
% evaldt : time duration
global dt;
time = 0;
u = [vt;wt];
traj = x;

while time <= evaldt
    time = time + dt;
    x = f(x, u);
    traj = [traj x];
end
end

%% function f
function x = f(x, u)
% motion modle
% u = [vt; wt] current v w
global dt;

F = [1 0 0 0 0
    0 1 0 0 0
    0 0 1 0 0
    0 0 0 0 0
    0 0 0 0 0];
B = [dt * cos(x(3)) 0
    dt * sin(x(3)) 0
    0 dt
    1 0
    0 1];

x = F * x + B * u;
end

%% function CalcHeadingEval
function heading = CalcHeadingEval(x, goal)
theta = rad2deg(x(3));

goalTheta = rad2deg(atan2(goal(2)-x(2),goal(1)-x(1)));

if goalTheta > theta
    targetTheta = goalTheta - theta;
else
    targetTheta = theta - goalTheta;
end

heading = 180 - targetTheta;
end

%% function CalcDistEval
function dist=CalcDistEval(x, ob, R)
dist = 2;
for io = 1:length(ob(:,1))
    disttmp = norm(ob(io,:) - x(1:2)') - R; 
    if dist > disttmp
        dist = disttmp;
    end
end
end
%% function calculate breaking distance
function stopDist = CalcBreakingDist(vel, model)
global dt;
stopDist = 0;
while vel > 0
    stopDist = stopDist + vel*dt;
    vel = vel - model(3)*dt;
end
end

%% rad/deg utilities
function rad = deg2rad(deg)
rad = deg / 180 * pi;
end

function deg = rad2deg(rad)
deg = rad * 180 / pi;
end